
from status import views
from django.urls import path,include

urlpatterns = [
    path('', views.redirect,name = 'redirect'),
    path('Story7/status/',views.index,name = "status"),
    path('Story7/confirm/',views.confirm,name = "confirm"),
    path('Story7/delete',views.delete,name="delete")
]
