from django.db import models
from datetime import datetime

# Create your models here.
class Statuses(models.Model):
    status = models.CharField(max_length = 300,default = "I'm good")
    date = models.DateTimeField(default=datetime.now, blank=True)