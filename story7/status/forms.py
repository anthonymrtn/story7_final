from django import forms
from .models import Statuses
class StatusForm(forms.Form):
    status = forms.CharField(max_length = 300, widget = forms.Textarea)
    
    class Meta:
        model = Statuses
        fields = ('status')